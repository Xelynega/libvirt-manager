#ifndef SERVER_H
#define SERVER_H

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <cstdlib>

#define SERVER_THREAD_BUFFER_SIZE 3000

enum server_errors {
	ERR_SOCKET_NOT_OPENED,
	ERR_SOCKET_FAILED_BIND,
	ERR_SOCKET_FAILED_LISTEN,
	ERR_STARTED_RUNNING_SERVER
};

struct server_thread
{
	pthread_t       thread;
	int             thread_socket;
	struct sockaddr client_address;
	char            buffer[SERVER_THREAD_BUFFER_SIZE];
	bool            finished = false;
	server_thread*  next = nullptr;
};

typedef void*(*client_handler)(void*);

class server
{
	public:
		server(int port, client_handler client_func);

		int                 initialize();
		int                 server_loop();
		
		pthread_t*          m_server_thread;
	private:
		int                 handle_client(int socket_number, struct sockaddr client_address);

		bool                m_running;
		client_handler      m_client_handler;
		int                 m_socket;
		int                 m_port;
		server_thread*      m_threads;
};

int server_thread_starter(server* tcp_server);

#endif
