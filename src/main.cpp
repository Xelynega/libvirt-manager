#include "libvirt-manager/main.h"

#include <iostream>

#include <sys/socket.h>

#include <cstdio>
#include <unistd.h>
#include <regex.h>

bool http_header_end(char* buffer)
{
	for(size_t ii = 0; buffer[ii] != 0; ii++)
	{
		char c = buffer[ii];
		if(c == '\r')
		{
			char c1 = buffer[ii + 1];
			char c2 = buffer[ii + 2];
			char c3 = buffer[ii + 3];

			if(c1 == '\n' && c2 == '\r' && c3 == '\n')
			{
				return true;
			}
		}
	}
	return false;
}

void* handle_clients(void* data)
{
	server_thread* current_thread = reinterpret_cast<server_thread*>(data);
	
	int buffer_pointer = 0;
	while(true)
	{
		buffer_pointer += recv(current_thread->thread_socket, &current_thread->buffer[buffer_pointer], SERVER_THREAD_BUFFER_SIZE - buffer_pointer, 0);
		if(http_header_end(current_thread->buffer))
		{
			break;
		}
	}
	printf("HTTP Header Data: %s\n", current_thread->buffer);
	memset(current_thread->buffer, 0, sizeof(current_thread->buffer));	
	current_thread->finished = true;
	return 0;
}

#include <string> 

int main(int argc, const char* argv[])
{
	if(argc < 2)
	{
		std::cout << "No port given" << std::endl;
		return -1;
	}

	std::cout << "Starting server on port " << atoi(argv[1]) << std::endl;

	server test_server(atoi(argv[1]), handle_clients);		
	int ret = test_server.initialize();

	printf("socket initialization returned %d\n", ret);

	if(ret != 0)
	{
		return -1;
	}

	test_server.server_loop();

	return 0;
}
