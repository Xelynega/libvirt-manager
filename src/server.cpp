#include "server/server.h"

#include <unistd.h>

server::server(int port, client_handler client_func)
{
	m_port = port;
	m_client_handler = client_func;
	m_running = false;
}

int server::initialize()
{
	m_socket = socket(AF_INET, SOCK_STREAM, 0);
	
	if(m_socket < 0)
	{
		m_socket = 0x00;
		return ERR_SOCKET_NOT_OPENED;
	}

	struct sockaddr_in serv_addr;
	memset((char*)&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(m_port);

	if(bind(m_socket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		m_socket = 0x00;
		return ERR_SOCKET_FAILED_BIND;
	}
	
	if(listen(m_socket, 10) < 0)
	{
		return ERR_SOCKET_FAILED_LISTEN;
	}
	m_threads = new server_thread;
	m_running = false;
	return 0;
}


// Clean up dead server threads, create new server thread and add it to the list
int server::handle_client(int socket_number, struct sockaddr client_address)
{
	server_thread* ptr = m_threads;

	while(ptr->next != nullptr)
	{
		if(ptr->next->finished)
		{
			server_thread* del_ptr = ptr->next;
			ptr->next = del_ptr->next;
			pthread_join(del_ptr->thread, nullptr);
			close(del_ptr->thread_socket);
			delete del_ptr;
		}
		if(ptr->next != nullptr)
		{
			ptr = ptr->next;
		}
	}

	server_thread* new_thread = new server_thread;
	
	ptr->next = new_thread;

	new_thread->thread_socket = socket_number;
	new_thread->client_address = client_address;
	memset(&new_thread->buffer, 0, sizeof(new_thread->buffer));
	new_thread->finished = false;
	new_thread->next = nullptr;

	pthread_create(&new_thread->thread, NULL, m_client_handler, (void*)new_thread);

	return 0;
}

int server::server_loop()
{
	if(m_running == true)
	{
		return ERR_STARTED_RUNNING_SERVER;
	}

	m_running = true;
	socklen_t addr_len = sizeof(struct sockaddr);
	
	while(m_running)
	{
		struct sockaddr client_addr;
		int client_socket_fd = accept(m_socket, &client_addr, &addr_len);
		if(client_socket_fd != -1)
		{
			handle_client(client_socket_fd, client_addr);
		}
	}

	return 0;
}

void* server_thread_starter(void* tcp_server)
{
	int ret = reinterpret_cast<server*>(tcp_server)->server_loop();
	return reinterpret_cast<void*>(ret);
}
